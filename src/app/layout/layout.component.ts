import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../services/services';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor(
    public theme: ThemeService,
  ) {
  }

  ngOnInit() {
  }

}
