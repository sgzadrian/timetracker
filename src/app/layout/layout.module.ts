import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutRoutingModule } from './layout.routing';
import { HomeComponent } from '../home/home.component';
import { TranslateModule } from '@ngx-translate/core';
import { ProjectComponent } from '../project/project.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ComponentsModule } from '../components/components.module';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    HomeComponent,
    ProjectComponent,
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    TranslateModule,
    ReactiveFormsModule,
    NgbModalModule,
    ComponentsModule,
    PipesModule,
  ]
})
export class LayoutModule { }
