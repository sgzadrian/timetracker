import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ElectronService } from 'ngx-electron';
import * as moment from 'moment';

import { Project, Entry, History } from '../../classes/Project';

@Injectable({ providedIn: 'root' })
export class ProjectService {

  private projectsEmitter = new BehaviorSubject<Array<any>>([]);
  private projectsData: Array<Project> = [];

  private entryFormat = 'YYYY/MM/DD HH:mm:ss';

  constructor(
    private router: Router,
    private electron: ElectronService,
    private ngZone: NgZone,
  ) {
    if ( electron.ipcRenderer ) {
      const eventKey = 'projects';
      electron.ipcRenderer.on( eventKey, ( evt: any, data: any ) => {
        ngZone.runTask( () => {
          this.projectsData = data;
          this.restore();
        } );
      } );
      ngZone.runOutsideAngular( () => {
        electron.ipcRenderer.send( 'getData', eventKey );
      } );
    }
    // console.log( this.projectsData );
  }

  restore() {
    this.emit();
    if ( this.projectsData.length ) {
      let id = this.projectsData[0].id;
      if ( localStorage.lastProject ) {
        id = localStorage.lastProject;
      }
      this.router.navigate([ '/project', id ]);
      localStorage.lastProject = id;
    }
  }

  get projects(): Observable<Array<any>> {
    return this.projectsEmitter;
  }

  private emit() {
    this.projectsEmitter.next( this.projectsData );
  }

  private save() {
    if ( this.electron.ipcRenderer ) {
      this.ngZone.runOutsideAngular(() => {
        this.electron.ipcRenderer.send( 'setData', 'projects', this.projectsData );
      });
    }
  }

  getProject( id: string ) {
    // const position = parseInt( id, 10 );
    // return this.projectsData[ position - 1 ];
    return this.projectsData.filter(( project: any ) => project.id === parseInt( id, 10 ) )[0];
  }

  addProject() {
    if ( !this.projectsData ) {
      this.projectsData = [];
    }
    const id = this.projectsData.length ? this.projectsData[ this.projectsData.length - 1 ].id + 1 : 1;
    const project = new Project();
    project.id = id;
    this.projectsData.push( project );
    this.save();
    this.emit();
    this.router.navigate([ '/project', id ]);
  }

  updateProject( project: Project ) {
    const pos = project.id - 1;
    this.projectsData[ pos ] = project;
    this.save();
    this.emit();
  }

  deleteProject( id: number ) {
    this.projectsData = this.projectsData.filter(( project: any ) => project.id !== id );
    this.save();
    this.emit();
    this.router.navigateByUrl( '/home' );
  }

  /* start & end should be moment.js variable */
  addEntry( project: Project, start: any, end: any ) {
    if ( !project.current ) { project.current = []; }
    if ( !project.hours ) { project.hours = 0; }
    const id = project.current.length + 1;
    const duration = end.diff( start, 'hours', true );
    project.hours += duration;
    const entry = new Entry(
      id,
      start.format( this.entryFormat ),
      end.format( this.entryFormat ),
      duration
    );
    project.current.push( entry );
    this.save();
  }

  deleteEntry( project: Project, id: number ) {
    project.current = project.current.filter(( entry: Entry ) => {
      const res = entry.id === id;
      if ( res ) {
        project.hours -= entry.duration;
      }
      return entry.id !== id;
    });
    this.save();
  }

  /* mow should be a moment.js variable */
  moveToHistory( project: Project, now: any ) {
    if ( !project.history ) { project.history = []; }
    const history = new History();
    history.date = now.format( this.entryFormat );
    history.duration = project.hours;
    project.hours = 0;
    history.entries = project.current;
    project.current = [];
    project.history.push( history );
    this.save();
  }

  // TODO: Handle import too and finish exporting
  exportData() {
    if ( this.electron.ipcRenderer ) {
      this.ngZone.runOutsideAngular(() => {
        this.electron.ipcRenderer.send( 'exportData', JSON.stringify( this.projectsData ) );
      });
    }
  }

  generateReport() {
    console.log( this.projectsData );
    let reportData = '';
    let total = 0;
    this.projectsData.map(( project: Project ) => {
      reportData += `${ project.name } (${ this.parseHours( project.hours ) })\n`;
      total += project.hours;
      project.current.map(( entry: Entry ) => {
        reportData += `\tFrom: ${ entry.start } \tTo: ${ entry.end }) \n`;
      });
      reportData += '\n';
    });
    reportData += `\n Total: ${ this.parseHours( total ) }`;
    if ( this.electron.ipcRenderer ) {
      this.ngZone.runOutsideAngular(() => {
        this.electron.ipcRenderer.send( 'generateReport', moment().format('[report_]DDMMYY[_]hhmmss[.txt]'), reportData );
      });
    }
  }

  private parseHours( value: number ) {
    if ( value <= 0 ) { return '0hrs'; }
    const hours = Math.floor( value );
    value = ( value - hours ) * 60;
    const minutes = Math.floor( value );
    value = ( value - minutes ) * 60;
    const seconds = Math.round( value );
    const duration = `${ hours }h ${ minutes }m ${ seconds }s`;
    return duration;
  }

}
