import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError, of } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class ApiService {

  base_url = environment.base_url;

  private options = { headers: null, };

  private _auth = new BehaviorSubject<boolean>( false );

  constructor(
    private http: HttpClient,
  ) {
    this.resetHeaders();
  }

  get auth(): Observable<boolean> { return this._auth; }

  private resetHeaders() {
    this.options.headers = new HttpHeaders({ 'Content-Type': 'application/json', });
  }

  private setToken( token: string ) {
    localStorage.setItem( 'token', token );
    this.options.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Token: token,
    });
  }

  restoreAccess() {
    const token = localStorage.getItem( 'token' );
    if ( token && token.length ) {
      this.setToken( token );
      this._auth.next( true );
    }
  }

  login( data: any ) {
    this.post( '/login', data ).subscribe(
      ( resp: any ) => {
        this.setToken( resp.token );
        this._auth.next( true );
      }
    );
  }

  logout() {
    localStorage.removeItem( 'token' );
    this.resetHeaders();
    this._auth.next( false );
  }

  private handleError( error: HttpErrorResponse ) {
    if ( error.error instanceof ErrorEvent ) {
      // Client-side or network error
      console.error( 'An error occurred:', error.error.message );
    } else {
      console.error( `Backend returned code ${error.status}, body was: ${error.message}` );
    }
    // return throwError( error.message );
  }

  private catchRequestError() {
    return catchError(( err: HttpErrorResponse ) => {
      if ( err.status === 401 ) {
        this.logout();
      } else {
        this.handleError( err );
      }
      return of( null );
    });
  }

  fetch( endpoint: string ) {
    return this.http.get( this.base_url + endpoint, this.options ).pipe(
      retry(2),
      this.catchRequestError()
    );
  }

  get( endpoint: string, id: any ) {
    return this.http.get( this.base_url + endpoint + '/' + id, this.options ).pipe(
      retry(2),
      this.catchRequestError()
    );
  }

  post( endpoint: string, data = null ) {
    return this.http.post( this.base_url + endpoint, data, this.options ).pipe(
      retry(2),
      this.catchRequestError()
    );
  }

  put( endpoint: string, id: any, data = null ) {
    return this.http.put( this.base_url + endpoint + '/' + id, data, this.options ).pipe(
      retry(2),
      this.catchRequestError()
    );
  }

  patch( endpoint: string, id: any, data = null ) {
    return this.http.patch( this.base_url + endpoint + '/' + id, data, this.options ).pipe(
      retry(2),
      this.catchRequestError()
    );
  }

  delete( endpoint: string, id: any ) {
    return this.http.delete( this.base_url + endpoint + '/' + id, this.options ).pipe(
      retry(2),
      this.catchRequestError()
    );
  }

}
