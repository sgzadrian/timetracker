import { Injectable, NgZone } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ElectronService } from 'ngx-electron';

@Injectable({ providedIn: 'root' })
export class ThemeService {

  theme = new BehaviorSubject<boolean> ( false );
  current = false;

  constructor(
    private electron: ElectronService,
    private ngZone: NgZone,
  ) {
    if ( electron.ipcRenderer ) {
      electron.ipcRenderer.on( 'themeChanged', ( evt: any, value: any ) => {
        ngZone.runTask( () => {
          this.current = value;
          this.save();
          this.emit();
        } );
      } );

      const eventKey = 'theme';
      electron.ipcRenderer.on( eventKey, ( evt: any, data: any ) => {
        ngZone.runTask( () => {
          this.current = data;
          this.emit();
        } );
      } );
      ngZone.runOutsideAngular( () => {
        electron.ipcRenderer.send( 'getData', eventKey );
      } );
    }
  }

  get isDark(): Observable<boolean> { return this.theme; }

  emit() { this.theme.next( this.current ); }

  private save() {
    if ( this.electron.ipcRenderer ) {
      this.ngZone.runOutsideAngular( () => {
        this.electron.ipcRenderer.send( 'setData', 'theme', this.current );
      } );
    }
  }

  toggle() {
    this.current = !this.current;
    this.save();
    this.emit();
  }

}
