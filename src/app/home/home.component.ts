import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ProjectService, ThemeService } from '../services/services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private router: Router,
    public projectService: ProjectService,
    public theme: ThemeService,
  ) {
  }

  ngOnInit() {
    // this.subs.add( this.projectService.projects.subscribe(
      // ( projects: any ) => {
        // if ( projects.length ) {
          // this.router.navigate([ 'project', projects[0].id ]);
        // }
      // }
    // ) );
  }

}
