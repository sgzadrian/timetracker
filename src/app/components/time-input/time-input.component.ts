import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import * as moment from 'moment/moment';

@Component({
  selector: 'app-time-input',
  templateUrl: './time-input.component.html',
  styleUrls: ['./time-input.component.scss'],
})
export class TimeInputComponent implements OnInit {

  months = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

  date = {
    d: '01', m: '01', y: '2019',
  };

  time = {
    h: '00', m: '00', s: '00',
  };

  @Input() label = '';
  @Input() extra: number;

  constructor(
  ) {
  }

  ngOnInit() {
    const now = moment();
    if ( this.extra ) { now.add( 1, 'h' ); }
    this.date.d = now.format( 'DD' );
    this.date.m = now.format( 'MM' );
    this.date.y = now.format( 'YYYY' );
    this.time.h = now.format( 'HH' );
    this.time.m = now.format( 'mm' );
    // this.time.s = now.format( 'ss' );
  }

  getMaxDate() { return this.months[ parseInt( this.date.m, 10 ) - 1 ]; }

  value() {
    const d = this.date;
    const t = this.time;
    const date = [ d.y, d.m, d.d, t.h, t.m, t.s ];
    return date;
  }

  format( val: number ) {
    let res = '00';
    if ( val < 10 ) {
      res = '0' + val;
    } else {
      res = '' + val;
    }
    return res;
  }

  onChange( evt: any ) {
    let val = evt.target.value;
    if ( val.length > 2 ) { val = val.slice( -2 ); }
    val = parseInt( val, 10 );
    evt.target.value = this.format( val );
  }

}
