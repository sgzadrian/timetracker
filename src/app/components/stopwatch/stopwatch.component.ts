import { Component, Input } from '@angular/core';
import * as moment from 'moment/moment';
import { ProjectService, ThemeService } from '../../services/services';

@Component({
  selector: 'app-stopwatch',
  templateUrl: './stopwatch.component.html',
  styleUrls: ['./stopwatch.component.scss']
})
export class StopwatchComponent {

  hours = 0;
  minutes = 0;
  seconds = 0;

  active = false;
  t = null;

  startTime = null;

  @Input() project;

  constructor(
    private projectService: ProjectService,
    public theme: ThemeService,
  ) {
  }

  toggleState() {
    this.active = !this.active;
    if ( this.active ) {
      this.startTime = moment();
      this.timer();
    } else {
      const endTime = moment();
      this.projectService.addEntry( this.project, this.startTime, endTime );
      this.startTime = null;
      clearTimeout( this.t );
      this.hours = 0;
      this.minutes = 0;
      this.seconds = 0;
    }
  }

  // TODO: Evaluate which method requires less resource
  add() {
    if ( !this.active ) { return; }
    const now = moment();
    /* Method #1 */
    // let diff = now.diff( this.startTime, 'seconds' );
    // if ( diff < 60 ) {
      // this.seconds = diff;
    // } else if ( diff < 3600 ) {
      // this.minutes = Math.floor( diff / 60 );
      // this.seconds = diff - ( this.minutes * 60 );
    // } else if ( diff >= 86399 ) {
      // this.hours = Math.floor( diff / 3600 );
      // diff -= this.hours;
      // this.minutes = Math.floor( diff / 60 );
      // this.seconds = diff - ( this.minutes * 60 );
      // this.toggleState();
      // this.toggleState();
    // }

    /* Method #2 */
    let diff = now.diff( this.startTime, 'hours', true );
    console.log(diff);
    this.hours = Math.floor( diff );
    diff = ( diff - this.hours ) * 60;
    this.minutes = Math.floor( diff );
    diff = ( diff - this.minutes ) * 60;
    this.seconds = Math.floor( diff );
    this.timer();
  }

  timer() {
    this.t = setTimeout(() => this.add(), 1000 );
  }

}
