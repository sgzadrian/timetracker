import { Component } from '@angular/core';
import { ProjectService, ThemeService } from '../../services/services';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

  constructor(
    public projectService: ProjectService,
    public theme: ThemeService,
  ) {
  }

}
