import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';

import { PipesModule } from '../pipes/pipes.module';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TimeInputComponent } from './time-input/time-input.component';
import { StopwatchComponent } from './stopwatch/stopwatch.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    PipesModule,
  ],
  declarations: [
    SidebarComponent,
    TimeInputComponent,
    StopwatchComponent,
    NavbarComponent,
  ],
  exports: [
    SidebarComponent,
    TimeInputComponent,
    StopwatchComponent,
    NavbarComponent,
  ]
})
export class ComponentsModule { }
