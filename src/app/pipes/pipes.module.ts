import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HoursPipe } from './hours/hours.pipe';
import { StringPipe } from './string/string.pipe';

@NgModule({
  declarations: [
    HoursPipe,
    StringPipe,
  ],
  exports: [
    HoursPipe,
    StringPipe,
  ]
})
export class PipesModule { }
