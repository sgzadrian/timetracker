import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'hours' })
export class HoursPipe implements PipeTransform {

  transform( value: any ): any {
    if ( !value ) { return '0h'; }
    const hours = Math.floor( value );
    value = ( value - hours ) * 60;
    const minutes = Math.floor( value );
    value = ( value - minutes ) * 60;
    const seconds = Math.round( value );
    const duration = `${ hours }h ${ minutes }m ${ seconds }s`;
    return duration;
  }

}
