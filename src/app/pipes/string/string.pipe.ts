import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'string' })
export class StringPipe implements PipeTransform {

  transform( value: string, ...args: any[] ): any {
    const size = args[0];
    if ( size && value.length >= size ) {
      value = value.substring( 0, size );
    }
    return value;
  }

}
