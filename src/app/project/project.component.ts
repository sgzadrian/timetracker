import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProjectService, ThemeService } from '../services/services';
import { FormControl } from '@angular/forms';
import { NgbModalConfig, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { SubSink } from 'subsink';
import * as moment from 'moment/moment';

import { StopwatchComponent } from '../components/stopwatch/stopwatch.component';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss'],
})
export class ProjectComponent implements OnInit, OnDestroy {

  @ViewChild( StopwatchComponent, { static: false } ) private stopwatch: StopwatchComponent;

  subs = new SubSink();

  project: any;

  name = new FormControl('');
  nameTimer = null;

  dateError = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService,
    private modalConf: NgbModalConfig,
    private modalService: NgbModal,
    private router: Router,
    public theme: ThemeService,
  ) {
    // modalConf.backdrop = true;
    // modalConf.keyboard = false; // Close with ESC
  }

  ngOnInit() {
    this.subs.add( this.activatedRoute.paramMap.subscribe( ( data: any ) => {
      if ( this.stopwatch && this.stopwatch.active ) {
        this.stopwatch.toggleState();
      }
      this.project = this.projectService.getProject( data.params.id );
      if ( !this.project ) {
        this.router.navigateByUrl( '/home' );
        return;
      }
      this.name.setValue( this.project.name );
      localStorage.lastProject = this.project.id;
    } ) );
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  updateName() {
    clearTimeout( this.nameTimer );
    this.nameTimer = setTimeout(() => {
      const val = this.name.value;
      this.project.name = val.length ? this.name.value : 'Untitled';
      this.projectService.updateProject( this.project );
    }, 1000);
  }

  open( modal: NgbModalRef ) {
    this.modalService.open( modal );
  }

  save( start, end ) {
    start = moment( start.value() ).subtract( 1, 'month' );
    end = moment( end.value() ).subtract( 1, 'month' );
    const startTime = start.format( 'HHmmss' );
    const endTime = end.format( 'HHmmss' );
    if (
      ( start.isSame( end ) && startTime < endTime )
      ||
      ( start.isBefore( end ) )
    ) {
      this.dateError = false;
      this.projectService.addEntry( this.project, start, end );
      this.modalService.dismissAll();
      return;
    }
    this.dateError = true;
  }

  deleteEntry( id: number ) {
    this.projectService.deleteEntry( this.project, id );
  }

  archiveCurrent() {
    this.projectService.moveToHistory( this.project, moment() );
  }

  deleteProject() {
    this.projectService.deleteProject( this.project.id );
    this.modalService.dismissAll();
  }

}
