import * as moment from 'moment/moment';

class Entry {
  id: number;
  start: string;
  end: string;
  duration: number;

  constructor( id: number, start: string, end: string, duration: number ) {
    this.id       = id;
    this.start    = start;
    this.end      = end;
    this.duration = duration;
  }
}

class History {
  id: number;
  date: string;
  duration: number;
  entries: Array<Entry>;
}

class Project {
  id: number;
  name = 'New Project';
  current: Array<Entry>;
  history: Array<History>;
  hours: number;
}

export {
  Project,
  Entry,
  History,
};
