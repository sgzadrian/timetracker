![TimeTracker](https://gitlab.com/sgzadrian/timetracker/raw/master/images/TimeTracker.jpg?inline=false)

This is a basic time tracking app created with Angular 8 & Electron, it includes typography, icons and 2 themes. 

## Terminal Commands

1. Install NodeJs from [NodeJs Official Page](https://nodejs.org/en).
2. Open Terminal
3. Go to your file project
4. Run in terminal: ```npm install -g @angular/cli```
5. Then: ```npm install```
6. And: ```npm run start```
7. Navigate to [localhost:4200](localhost:4200)

To compile it follow steps from 1 to 6 and then:

7. ```npm run elec-dist-mac```
8. Navigate to project folder and check the final app into dist
