const { app, BrowserWindow, systemPreferences, ipcMain, Notification } = require('electron');
const path = require('path');
const url = require('url');
const fs = require('fs');

const Store = require('./electron/store.ts');

/* Enable Production Mode */
process.env.NODE_ENV = 'production';

/* Declarations */
const isMac = process.platform === 'darwin';
const appName = 'TimeTracker';
const store = new Store();
const basePath = process.env.APPDATA || process.env.HOME;
let mainWindow = null;

/* Events */
app.on('ready', () => { createWindow(); });

app.on('window-all-closed', () => {
  if ( process.platform !== 'darwin' ) { app.quit(); }
});

app.on('activate', () => {
  if ( mainWindow === null ) { createWindow(); }
});

ipcMain.on('getData', ( event, key ) => {
  mainWindow.webContents.send( key, store.getData( key ) );
});

ipcMain.on('setData', ( event, key, data ) => {
  store.setData( key, data );
});

ipcMain.on('exportData', ( event, data ) => {
  const filePath = `${ basePath }/Downloads/TimeTrackerData.json`;
  fs.writeFileSync( filePath, data );
  const notify = new Notification({
    title: 'Data Exported',
    body: 'Check your Downloads folder',
  });
  notify.show();
});

ipcMain.on('generateReport', ( event, filename, data ) => {
  const filePath = `${ basePath }/Downloads/${ filename }`;
  fs.writeFileSync( filePath, data );
  const notify = new Notification({
    title: 'Report Generated',
    body: 'Check your Downloads folder',
  });
  notify.show();
});

/* Handle dark mode on Mac */
systemPreferences.subscribeNotification(
  'AppleInterfaceThemeChangedNotification', () => {
    const isDark = systemPreferences.isDarkMode();
    // mainWindow.setVibrancy( isDark ? 'dark' : 'light' );
    mainWindow.webContents.send( 'themeChanged', isDark );
  });

/* Main Function */
function createWindow() {
  mainWindow = new BrowserWindow({
    show: false,
    width: 800,
    height: 600,
    minWidth: 600,
    minHeight: 320,
    fullscreen: false,
    center: true,
    titleBarStyle: 'hidden',
    webPreferences: {
      nodeIntegration: true,
    },
    // skipTaskbar: true,
  });

  mainWindow.loadURL( url.format({
    pathname: path.join( __dirname, '/dist/index.html' ),
    protocol: 'file:',
    slashes: true
  }) );

  // Open DevTools
  if ( process.env.NODE_ENV !== 'production' ) { mainWindow.openDevTools(); }

  // Garbage collect, here must destroy app windows
  mainWindow.on( 'closed', () => { app.exit(0); } );

  mainWindow.once( 'ready-to-show', () => { mainWindow.show(); } );
}

