const { app, remote } = require('electron');
const fs = require('fs');
const path = require('path');

class Store {

  path;
  data;

  constructor() {
    this.path = process.env.APPDATA || process.env.HOME + '/.config/timetracker.json';
    this.data = readData( this.path );
  }

  getData( key ) {
    return this.data[ key ];
  }

  setData( key, val ) {
    this.data[key] = val;
    fs.writeFileSync( this.path, JSON.stringify( this.data ) );
  }

}

function readData( filePath ) {
  try {
    return JSON.parse( fs.readFileSync( filePath ) );
  } catch ( error ) {
    return {};
  }
}

module.exports = Store;
